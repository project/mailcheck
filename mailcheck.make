core = 7.x
api = 2

; Libraries
libraries[mailcheck][download][type] = file
libraries[mailcheck][download][url] = https://cdn.rawgit.com/tableau-mkt/mailcheck/master/src/mailcheck.js
libraries[mailcheck][download][filename] = mailcheck.js
libraries[mailcheck][directory_name] = mailcheck
libraries[mailcheck][destination] = libraries
